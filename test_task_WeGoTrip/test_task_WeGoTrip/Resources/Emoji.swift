import Foundation

enum Emoji: String {
  case veryBad = "😡"
  case bad = "☹️"
  case normal = "😐"
  case good = "🙂"
  case veryGood = "😁"
}
