import Foundation

struct FeedbackData {
  var id = Int.random(in: 1000...9999)
  var averageGrade: Int
  var likeTour: String
  var improveTour: String
}
