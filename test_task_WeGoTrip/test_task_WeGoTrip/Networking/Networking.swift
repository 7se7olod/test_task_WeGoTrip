import UIKit

struct Networking {
  // MARK: - properties
  private static let urlImage = "https://app.wegotrip.com/media/users/1/path32.png"
  private static let urlString = "https://webhook.site/c8f2041c-c57e-433f-853f-1ef739702903"

  private static var imageCache = NSCache<NSString, UIImage>()
  private static var urlSession = URLSession.shared

  // MARK: - sendPostRequest
  static func sendPostRequest(data: FeedbackData) {
    guard let url = URL(string: urlString) else { return }
    var request = URLRequest(url: url)
    request.httpMethod = "POST"

    let feedbackData: [String: Any] = [
      "tourID": data.id,
      "averageGrade": data.averageGrade,
      "likeTourText" : data.likeTour,
      "improveTourText": data.improveTour
    ]

    guard let httpBody = try? JSONSerialization.data(withJSONObject: feedbackData, options: []) else { return }

    request.httpBody = httpBody

    print(httpBody)

    let task = urlSession.dataTask(with: request) { data, response, error in
      if let error = error {
        print(error.localizedDescription)
        return
      }

      guard let response = response as? HTTPURLResponse else { return }
      print(response.statusCode)

      guard let data = data else { return }
      print(data)
    }
    task.resume()
  }

  // MARK: - Loading Image
  static func loadImage(imageView: UIImageView) {

    guard let url = URL(string: urlImage) else { return }

    if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
      imageView.image = cachedImage
    } else {
      let dataTask = URLSession.shared.dataTask(with: url) { data, response, error in
        if let error = error {
          print(error.localizedDescription)
          return
        }

        guard let response = response as? HTTPURLResponse else { return }
        print(response.statusCode)

        DispatchQueue.main.async {
          guard let data = data,
                let dataImage = UIImage(data: data) else { return }
          imageView.image = dataImage
          self.imageCache.setObject(dataImage, forKey: url.absoluteString as NSString)
        }
      }
      dataTask.resume()
    }
  }
}
