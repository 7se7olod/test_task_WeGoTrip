import UIKit

class QuestionsViewController: UIViewController {

  // MARK: - ImageView
  @IBOutlet var imageView: UIImageView!

  // MARK: - Slider
  @IBOutlet var firstSlider: UISlider!
  @IBOutlet var secondSlider: UISlider!
  @IBOutlet var thirdSlider: UISlider!
  @IBOutlet var fourSlider: UISlider!

  // MARK: - UILabel
  @IBOutlet var firstEmojiLabel: UILabel!
  @IBOutlet var secondEmojiLabel: UILabel!
  @IBOutlet var thirdEmojiLabel: UILabel!
  @IBOutlet var fourEmojiLabel: UILabel!

  // MARK: - LifeCycle
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setValueSliders()
    navigationController?.setNavigationBarHidden(true, animated: animated)
    imageView.layer.cornerRadius = imageView.frame.width / 2
    Networking.loadImage(imageView: self.imageView)
  }

  // MARK: - IBActions
  @IBAction func firstSliderTapped(_ sender: UISlider) {
    sender.setValue(Float(lroundf(firstSlider.value)), animated: true)
    setEmojiLabel(label: firstEmojiLabel, value: sender.value)
  }

  @IBAction func secondSliderTapped(_ sender: UISlider) {
    sender.setValue(Float(lroundf(secondSlider.value)), animated: true)
    setEmojiLabel(label: secondEmojiLabel, value: sender.value)
  }

  @IBAction func thirdSliderTapped(_ sender: UISlider) {
    sender.setValue(Float(lroundf(thirdSlider.value)), animated: true)
    setEmojiLabel(label: thirdEmojiLabel, value: sender.value)
  }
  
  @IBAction func fourSliderTapped(_ sender: UISlider) {
    sender.setValue(Float(lroundf(fourSlider.value)), animated: true)
    setEmojiLabel(label: fourEmojiLabel, value: sender.value)
  }

  @IBAction func saveInfoTapped(_ sender: UIButton) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    guard let feedbackViewController = storyboard.instantiateViewController(withIdentifier: "FeedbackViewController") as? FeedbackViewController else { return }

    let sum = firstSlider.value + secondSlider.value + thirdSlider.value + fourSlider.value
    let averageValue = Int(sum) / 4

    feedbackViewController.feedbackData = FeedbackData(averageGrade: averageValue, likeTour: "", improveTour: "")
    navigationController?.pushViewController(feedbackViewController, animated: true)
  }

  // MARK: - Methods
  private func setValueSliders() {
    firstSlider.value = 3
    secondSlider.value = 3
    thirdSlider.value = 3
    fourSlider.value = 3
  }

  private func setEmojiLabel(label: UILabel, value: Float) {
    switch value {
      case 1:
        label.text = Emoji.veryBad.rawValue
      case 2:
        label.text = Emoji.bad.rawValue
      case 3:
        label.text = Emoji.normal.rawValue
      case 4:
        label.text = Emoji.good.rawValue
      case 5:
        label.text = Emoji.veryGood.rawValue
      default:
        break
    }
  }
}
