import UIKit

class FeedbackViewController: UIViewController {
  // MARK: - Property
  var feedbackData: FeedbackData?

  // MARK: - IBOutlets
  @IBOutlet var imageView: UIImageView!
  @IBOutlet var firstTextView: UITextView!
  @IBOutlet var secondTextView: UITextView!
  @IBOutlet var scrollView: UIScrollView!

  // MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    firstTextView.delegate = self
    secondTextView.delegate = self
    firstTextView.textColor = UIColor.lightGray
    secondTextView.textColor = UIColor.lightGray
    keyboardNotification()
    addTapGestureToHideKeyboard()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    Networking.loadImage(imageView: self.imageView)
  }

  // MARK: - IBAction
  @IBAction func saveFeedbackTapped(_ sender: UIButton) {
    if firstTextView.textColor != UIColor.lightGray && secondTextView.textColor != UIColor.lightGray {
      guard var data = feedbackData else { return }
      data.likeTour = firstTextView.text
      data.improveTour = secondTextView.text
      Networking.sendPostRequest(data: data)
      navigationController?.dismiss(animated: true, completion: nil)
    } else {
      presentAlertController()
    }
  }
}

extension FeedbackViewController {
  // MARK: - AlertController
  private func presentAlertController() {
    let alertController = UIAlertController(title: "Пустые поля", message: "Заполните пустые поля", preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: .cancel)
    alertController.addAction(okAction)
    present(alertController, animated: true)
  }

  // MARK: - Keyboard
  private func addTapGestureToHideKeyboard() {
    let tapGesture = UITapGestureRecognizer(target: view, action: #selector(view.endEditing))
    view.addGestureRecognizer(tapGesture)
  }

  private func keyboardNotification() {
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
  }

  @objc private func keyboardWillShow(notification:NSNotification) {
    guard let userInfo = notification.userInfo else { return }
    var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
    keyboardFrame = self.view.convert(keyboardFrame, from: nil)
    var contentInset:UIEdgeInsets = self.scrollView.contentInset
    contentInset.bottom = keyboardFrame.size.height
    scrollView.contentInset = contentInset
  }

  @objc private func keyboardWillHide(notification:NSNotification) {
    let contentInset:UIEdgeInsets = UIEdgeInsets.zero
    scrollView.contentInset = contentInset
  }
}

// MARK: - TextViewDelegate
extension FeedbackViewController: UITextViewDelegate {
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.textColor == UIColor.lightGray {
      textView.text = nil
      textView.textColor = UIColor.black
    }
  }

  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text.isEmpty {
      textView.text = "Напишите здесь, чем вам запомнился тур, посоветуете ли его друзьям и как удалось ли повеселиться"
      textView.textColor = UIColor.lightGray
    }
  }
}
