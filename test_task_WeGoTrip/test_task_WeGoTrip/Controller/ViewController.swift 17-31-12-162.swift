import UIKit

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  @IBAction func startQuestioning(_ sender: UIButton) {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let navigationController = storyboard.instantiateViewController(withIdentifier: "NavigationControllerID")
    present(navigationController, animated: true)
  }
}

